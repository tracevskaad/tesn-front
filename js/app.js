"use strict";

$(function () {
  var TESN = {
    frontSlider: function () {
      var $wrappItemFront = $('.front-block .front-slider .wrapp-item-fs'),
      $navFront = $('.front-block .front-slider .front-slider-nav-btn .nav-line .line'),
      numOfSlides = $wrappItemFront.find('.item-front-slider').length,
      slideNow = 1,
      navBtnId = 0,
      translateWidth = 0;

      $('.total-front-slide').text(numOfSlides);

      function activeNav(index) {
        $navFront.removeClass('active');
        $navFront.eq(index).addClass('active');
      }

      function activeItem (indexItem) {
        $('.active-front-slide').text(indexItem);
      }

      function nextSlide() {
        if (slideNow === numOfSlides || slideNow <= 0 || slideNow > numOfSlides) {
          $wrappItemFront.css('transform', 'translate(0, 0)');
          slideNow = 1;
          activeNav(0);
          activeItem(1);
        } else {
          activeNav(slideNow);
          translateWidth = -$wrappItemFront.width() * (slideNow);
          $wrappItemFront.css({
            'transform': 'translate(' + translateWidth + 'px, 0)'
          });
          slideNow++;
          activeItem(slideNow);
        }
      }

      activeNav(0);
      activeItem(1);

      /*Auto playing*/
      var switchInterval = setInterval(nextSlide, 3000);
      $wrappItemFront.hover(function() {
        clearInterval(switchInterval);
      }, function() {
        switchInterval = setInterval(nextSlide, 3000);
      });

      /*Click nav-btn*/
      $navFront.click(function() {
        navBtnId = $(this).index();
        $navFront.removeClass('active');
        $navFront.eq(navBtnId).addClass('active');
        if (navBtnId + 1 != slideNow) {
          translateWidth = -$wrappItemFront.width() * (navBtnId);
          $wrappItemFront.css({
            'transform': 'translate(' + translateWidth + 'px, 0)'
          });
          slideNow = navBtnId + 1;
          activeItem(slideNow);
        }
      });
    },
  }
  if($('.front-block .front-slider').length) {
    TESN.frontSlider();
  }
  if($('.wrapp-slider-case .slider-case').length) {
    TESN.sliderCase();
  }
});